<?php

namespace Afaqy\Core\Builders;

use Afaqy\Core\Builders\Containers\Container;
use Afaqy\Core\Builders\Contracts\BaseBuilder;

abstract class Builder implements BaseBuilder
{
    /**
     * Builder container
     * @var \Afaqy\Core\Builders\Containers\Container
     */
    private $container;

    /**
     * Dependencies array
     * @var array
     */
    private $dependencies = [];

    /**
     * Set builder container.
     * @param \Afaqy\Core\Builders\Containers\Container $container
     * @return void
     */
    public function setContainer(Container $container): void
    {
        $this->container = $container;
    }

    /**
     * Set builder dependency
     * @param string $key
     * @param mixed  $value
     */
    public function setDependency(string $key, $value): void
    {
        $this->dependencies[$key] = $value;
    }

    /**
     * Get builder container
     * @return \Afaqy\Core\Builders\Containers\Container
     */
    public function container(): Container
    {
        return $this->container;
    }

    /**
     * Get builder dependency
     * @param  string $key
     * @return mixed
     */
    public function dependency(string $key)
    {
        return $this->dependencies[$key];
    }

    /**
     * Get builder dependencies
     * @return array
     */
    public function dependencies(): array
    {
        return $this->dependencies;
    }

    /**
     * Check if builder has the given dependency key.
     * @param  string $key
     * @return boolean
     */
    public function availableDependency(string $key): bool
    {
        return array_key_exists($key, $this->dependencies);
    }

    /**
     * Remove the given dependency from the array of dependencies.
     * @param  string $dependency
     * @return boolean
     */
    public function forgetDependency(string $dependency): bool
    {
        if ($this->availableDependency($dependency)) {
            unset($this->dependencies[$dependency]);

            return true;
        }

        return false;
    }

    /**
     * Rebuild the given dependency.
     * @param  string $dependency
     * @return mixed
     *
     * @throws \Exception
     */
    public function rebuildDependency(string $dependency)
    {
        if (!$this->forgetDependency($dependency)) {
            throw new \Exception(sprintf(
                "Can't find dependency (%s) in the (%s) dependencies array.",
                $dependency,
                self::class
            ));
        }

        $this->setDependency($dependency, $this->container->build($dependency));

        return $this->dependency($dependency);
    }

    /**
     * Try to resolve the given dependency
     * @param  string $dependency
     * @return mixed
     */
    public function __get($dependency)
    {
        if (empty($this->container)) {
            throw new \Exception(sprintf(
                "Call undefined (%s) property. [Tip] try to set your container through setContainer() method.",
                $dependency
            ));
        }

        if (!$this->availableDependency($dependency)) {
            $this->setDependency($dependency, $this->container->build($dependency));
        }

        return $this->dependency($dependency);
    }
}
