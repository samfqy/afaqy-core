<?php

namespace Afaqy\Core\Builders\Containers;

abstract class Container
{
    /**
     * Set the builder dependencies.
     * @return array
     */
    abstract public function dependencies(): array;

    /**
     * Resolve the given dependency.
     * @param  string $dependency
     * @return mixed
     *
     * @throws \Exception
     */
    public function build(string $dependency)
    {
        if (array_key_exists($dependency, $this->dependencies())) {
            return resolve($this->dependencies()[$dependency]);
        }

        throw new \Exception(sprintf(
            "Can't find key (%s) in the (%s) dependencies array.",
            $dependency,
            get_class($this)
        ));
    }
}
