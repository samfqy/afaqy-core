<?php

namespace Afaqy\Core\Builders\Contracts;

use Afaqy\Core\Builders\Containers\Container;

interface BaseBuilder
{
    /**
     * Set builder container.
     * @param \Afaqy\Core\Builders\Containers\Container $container
     * @return void
     */
    public function setContainer(Container $container): void;

    /**
     * Set builder dependency
     * @param string $key
     * @param mixed  $value
     */
    public function setDependency(string $key, $value): void;

    /**
     * Get builder container
     * @return \Afaqy\Core\Builders\Containers\Container
     */
    public function container(): Container;

    /**
     * Get builder dependency
     * @param  string $key
     * @return mixed
     */
    public function dependency(string $key);

    /**
     * Get builder dependencies
     * @return array
     */
    public function dependencies(): array;

    /**
     * Check if builder has the given dependency key.
     * @param  string $key
     * @return boolean
     */
    public function availableDependency(string $key): bool;

    /**
     * Remove the given dependency from the array of dependencies.
     * @param  string $dependency
     * @return boolean
     */
    public function forgetDependency(string $dependency): bool;

    /**
     * Rebuild the given dependency.
     * @param  string $dependency
     * @return mixed
     *
     * @throws \Exception
     */
    public function rebuildDependency(string $dependency);
}
