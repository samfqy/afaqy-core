<?php

namespace Afaqy\Core\Caches;

use Illuminate\Cache\Repository;
use Illuminate\Database\Eloquent\Model;
use Afaqy\Core\Caches\Contracts\BaseCache;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

abstract class Cache implements BaseCache
{
    /**
     * Repository class.
     * @var \Afaqy\Core\Repositories\Contracts\BaseRepository
     */
    protected $repository;

    /**
     * @var Repository
     */
    protected $cache;

    /**
     * @var int caching time
     */
    protected $cacheTime;

    /**
     * @var string The entity name
     */
    protected $entityName;

    /**
     * @var string The application locale
     */
    protected $locale;

    /**
     * Repository model.
     * @var \Illuminate\Database\Eloquent\Model
     */
    private $model;

    public function __construct()
    {
        $this->cache     = app(Repository::class);
        $this->cacheTime = app(ConfigRepository::class)->get('cache.time', 60);
        $this->locale    = app()->getLocale();
    }

    public function clearCache()
    {
        $store = $this->cache;
        if (method_exists($this->cache->getStore(), 'tags')) {
            $store = $store->tags($this->entityName);
        }

        return $store->flush();
    }

    /**
     * @param \Closure $callback
     * @param null|string $key
     * @return mixed
     */
    protected function remember(\Closure $callback, $key = null)
    {
        $cacheKey = $this->makeCacheKey($key);
        $store    = $this->cache;
        if (method_exists($this->cache->getStore(), 'tags')) {
            $store = $store->tags([$this->entityName, 'global']);
        }

        return $store->remember($cacheKey, $this->cacheTime, $callback);
    }

    /**
     * Generate a cache key with the called method name and its arguments
     * If a key is provided, use that instead
     * @param null|string $key
     * @return string
     */
    private function makeCacheKey($key = null): string
    {
        if ($key !== null) {
            return $key;
        }
        $cacheKey  = $this->getBaseKey();
        $backtrace = debug_backtrace()[2];

        return sprintf("${cacheKey} %s %s", $backtrace['function'], \serialize($backtrace['args']));
    }

    /**
     * @return string
     */
    protected function getBaseKey(): string
    {
        return sprintf('afaqy -locale:%s -entity:%s', $this->locale, $this->entityName);
    }

    /**
     * Retrieve data from the database.
     * @param  array $columns
     * @param  array $options['orderBy', 'direction', 'filters']
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieve(array $columns = ['*'], array $options = []): Collection
    {
        return $this->remember(function () use ($columns, $options) {
            return $this->repository->retrieve($columns, $options);
        });
    }

    /**
     * Retrieve data paginated from the database.
     * @param  array $columns
     * @param  array $options['orderBy', 'direction', 'filters']
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrievePaginate(array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->remember(function () use ($columns, $options) {
            return $this->repository->retrievePaginate($columns, $options);
        });
    }

    /**
     * Retrieve all conditioning data from database.
     * @param  array $conditions
     * @param  array $columns
     * @param  array $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveBy(array $conditions, array $columns = ['*'], array $options = []): Collection
    {
        return $this->remember(function () use ($conditions, $columns, $options) {
            return $this->repository->retrieveBy($conditions, $columns, $options);
        });
    }

    /**
     * Retrieve conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $columns
     * @param  array $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByPaginate(array $conditions, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->remember(function () use ($conditions, $columns, $options) {
            $this->repository->retrieveBy($conditions, $columns, $options);
        });
    }

    /**
     * Retrieve all optional conditioning data from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveByOptional(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): Collection
    {
        return $this->remember(function () use ($conditions, $orConditions, $columns, $options) {
            return $this->repository->retrieveByOptional($conditions, $orConditions, $columns, $options);
        });
    }

    /**
     * Retrieve optional conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByOptionalPaginate(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->remember(function () use ($conditions, $orConditions, $columns, $options) {
            return $this->repository->retrieveByOptionalPaginate($conditions, $orConditions, $columns, $options);
        });
    }

    /**
     * Retrieve all data with their defined relation from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveWithRelations(array $relations, array $columns = ['*'], array $options = []): Collection
    {
        return $this->remember(function () use ($relations, $columns, $options) {
            return $this->repository->retrieveWithRelations($relations, $columns, $options);
        });
    }

    /**
     * Retrieve data with their defined relation paginated from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveWithRelationsPaginate(array $relations, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->remember(function () use ($relations, $columns, $options) {
            return $this->repository->retrieveWithRelationsPaginate($relations, $columns, $options);
        });
    }

    /**
     * Retrieve all joined data ASC from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoined(array $joins, array $columns = ['*'], array $options = []): Collection
    {
        return $this->remember(function () use ($joins, $columns, $options) {
            return $this->repository->retrieveJoined($joins, $columns, $options);
        });
    }

    /**
     * Retrieve joined data paginated from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedPaginate(array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->remember(function () use ($joins, $columns, $options) {
            return $this->repository->retrieveJoinedPaginate($joins, $columns, $options);
        });
    }

    /**
     * Retrieve all conditioning joined data from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoinedBy(array $conditions, array $joins, array $columns = ['*'], array $options = []): Collection
    {
        return $this->remember(function () use ($conditions, $joins, $columns, $options) {
            return $this->repository->retrieveJoinedBy($conditions, $joins, $columns, $options);
        });
    }

    /**
     * Retrieve all conditioning joined data from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedByPaginate(array $conditions, array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->remember(function () use ($conditions, $joins, $columns, $options) {
            return $this->repository->retrieveJoinedByPaginate($conditions, $joins, $columns, $options);
        });
    }

    /**
     * Find the given id.
     * @param int $id
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find(int $id, array $columns = ['*'], bool $trashed = false): Model
    {
        return $this->remember(function () use ($id, $columns, $trashed) {
            return $this->repository->find($id, $columns, $trashed);
        });
    }

    /**
     * Find the given id or fail.
     * @param int $id
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     *
     */
    public function findOrFail(int $id, array $columns = ['*'], bool $trashed = false): Model
    {
        return $this->remember(function () use ($id, $columns, $trashed) {
            return $this->repository->findOrFail($id, $columns, $trashed);
        });
    }

    /**
     * Create new record.
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data): Model
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->create($data);
    }

    /**
     * Update the given record id.
     * @param int   $id
     * @param array $data
     * @return int
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(int $id, array $data): int
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->update($id, $data);
    }

    /**
     * Update the given records ids.
     * @param array $ids
     * @param array $data
     * @return int
     */
    public function updateMany(array $ids, array $data): int
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->updateMany($ids, $data);
    }

    /**
     * Update data by conditions.
     * @param array $conditions
     * @param array $data
     * @return int
     */
    public function updateBy(array $conditions, array $data): int
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->updateBy($conditions, $data);
    }

    /**
     * Destroy the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function destroy(int $id):? bool
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->destroy($id);
    }

    /**
     * Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function destroyMany(array $ids): bool
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->destroyMany($ids);
    }

    /**
     * Restore the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function restore(int $id):? bool
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->restore($id);
    }

    /**
     * Restore the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function restoreMany(array $ids): bool
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->restoreMany($ids);
    }

    /**
     * Force Destroy the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function forceDestroy(int $id):? bool
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->forceDestroy($id);
    }

    /**
     * Force Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function forceDestroyMany(array $ids): bool
    {
        $this->cache->tags($this->entityName)->flush();

        return $this->repository->forceDestroyMany($ids);
    }
}
