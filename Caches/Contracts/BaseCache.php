<?php

namespace Afaqy\Core\Caches\Contracts;

use Afaqy\Core\Repositories\Contracts\BaseRepository;

interface BaseCache extends BaseRepository
{
    public function clearCache();
}
