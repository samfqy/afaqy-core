<?php

namespace Afaqy\Core\Http\Controllers;

use Afaqy\Core\Traits\FractalBuilder;
use Afaqy\Core\Traits\ResponseBuilder;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
    use ResponseBuilder, FractalBuilder;

    /**
     * @SWG\Swagger(
     *     basePath="/api",
     *     schemes={L5_SWAGGER_CONST_SCHEMES},
     *     host=L5_SWAGGER_CONST_HOST,
     *     @SWG\Info(
     *         version="2.0.0",
     *         title="Afaqy API Documentation",
     *         description="",
     *         @SWG\Contact(
     *             email="info@afaqy.com"
     *         ),
     *     )
     * )
     */

    /**
     * @SWG\Tag(
     *   name="Auth",
     *   description="Operations about Authentication",
     * )
     * @SWG\Tag(
     *   name="Setting",
     *   description="Operations about setting"
     * )
     * @SWG\Tag(
     *   name="User",
     *   description="Operations about user"
     * )
     * @SWG\Tag(
     *   name="Role",
     *   description="Manage roles and permissions."
     * )
     */
}
