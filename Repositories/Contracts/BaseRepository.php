<?php

namespace Afaqy\Core\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface BaseRepository
{
    /**
     * Retrieve all data from the database.
     * @param  array   $columns
     * @param  array   $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieve(array $columns = ['*'], array $options = []): Collection;

    /**
     * Retrieve data paginated from the database.
     * @param  array   $columns
     * @param  array   $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrievePaginate(array $columns = ['*'], array $options = []): LengthAwarePaginator;

    /**
     * Retrieve all conditioning data from database.
     * @param  array  $conditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveBy(array $conditions, array $columns = ['*'], array $options = []): Collection;

    /**
     * Retrieve conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByPaginate(array $conditions, array $columns = ['*'], array $options = []): LengthAwarePaginator;

    /**
     * Retrieve all optional conditioning data from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveByOptional(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): Collection;

    /**
     * Retrieve optional conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByOptionalPaginate(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): LengthAwarePaginator;

    /**
     * Retrieve all data with their defined relation from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveWithRelations(array $relations, array $columns = ['*'], array $options = []): Collection;

    /**
     * Retrieve data with their defined relation paginated from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveWithRelationsPaginate(array $relations, array $columns = ['*'], array $options = []): LengthAwarePaginator;

    /**
     * Retrieve all joined data ASC from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoined(array $joins, array $columns = ['*'], array $options = []): Collection;

    /**
     * Retrieve joined data paginated from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedPaginate(array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator;

    /**
     * Retrieve all conditioning joined data from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoinedBy(array $conditions, array $joins, array $columns = ['*'], array $options = []): Collection;

    /**
     * Retrieve conditioning joined data paginated from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedByPaginate(array $conditions, array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator;

    /**
     * Find the given id.
     * @param int $id
     * @param array $columns
     *
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find(int $id, array $columns = ['*'], bool $trashed = false): Model;

    /**
     * Find the given id or fail.
     * @param int $id
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     *
     */
    public function findOrFail(int $id, array $columns = ['*'], bool $trashed = false): Model;

    /**
     * Create new record.
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data): Model;

    /**
     * Update the given record id.
     * @param int   $id
     * @param array $data
     * @return int
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(int $id, array $data): int;

    /**
     * Update the given records ids.
     * @param array $ids
     * @param array $data
     * @return int
     */
    public function updateMany(array $ids, array $data): int;

    /**
     * Update data by conditions.
     * @param array $conditions
     * @param array $data
     * @return int
     */
    public function updateBy(array $conditions, array $data): int;

    /**
     * Destroy the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function destroy(int $id):? bool;

    /**
     * Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function destroyMany(array $ids): bool;

    /**
     * Restore the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function restore(int $id):? bool;

    /**
     * Restore the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function restoreMany(array $ids): bool;

    /**
     * Force Destroy the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function forceDestroy(int $id):? bool;

    /**
     * Force Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function forceDestroyMany(array $ids): bool;
}
