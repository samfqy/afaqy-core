<?php

namespace Afaqy\Core\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Afaqy\Core\Repositories\Contracts\BaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

abstract class Repository implements BaseRepository
{
    /**
     * Repository model.
     * @var \Illuminate\Database\Eloquent\Model
     */
    private $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * return query builder.
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function builder(): Builder
    {
        return $this->model->query();
    }

    /**
     * prefix the given column with model table name.
     * @param  string $column
     * @return string
     */
    protected function prefixTable(string $column): string
    {
        return (Str::contains($column, '.')) ? $column : $this->model->getTable() . '.' . $column;
    }

    /**
     * Set select columns.
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function select(array $columns = ['*']): Builder
    {
        return $this->builder()->selectRaw(implode(',', $columns));
    }

    /**
     * Load translations data if exist.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function loadTranslations(Builder $query): Builder
    {
        if (method_exists($this->model, 'translations')) {
            $query->with('translations');
        }

        // load translations by joins if translations eloquent relationship not exist.
        if (method_exists($this->model, 'scopeTranslations') && !array_key_exists('translations', $query->getEagerLoads())) {
            $query->translations();
        }

        return $query;
    }

    /**
     * Build Retrieve query.
     * @param array $columns
     * @param array $options
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function retrieveQuery(array $columns = ['*'], array $options = []): Builder
    {
        $builder = $this->select($columns)
            ->orderBy($this->prefixTable($options['sort'] ?? config('app.order_by')), $options['direction'] ?? config('app.direction'))
            ->limit($options['limit'] ?? -1);

        if (key_exists('joins', $options) && is_array($options['joins']) && count($options['joins'])) {
            foreach ($options['joins'] as $join) {
                $builder->{$join}();
            }
        }

        $builder = $this->loadTranslations($builder);

        if (key_exists('filters', $options)) {
            $builder->filter($options['filters'] ?? []);
        }

        if (key_exists('groupBy', $options)) {
            $builder->groupBy($options['groupBy']);
        }

        return $builder;
    }

    /**
     * Retrieve data from the database.
     * @param  array $columns
     * @param  array $options['orderBy', 'direction', 'filters']
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieve(array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->get();
    }

    /**
     * Retrieve data paginated from the database.
     * @param  array $columns
     * @param  array $options['orderBy', 'direction', 'filters']
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrievePaginate(array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->paginate($this->model->getPerPage());
    }

    /**
     * Retrieve all conditioning data from database.
     * @param  array $conditions
     * @param  array $columns
     * @param  array $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveBy(array $conditions, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->where($conditions)->get();
    }

    /**
     * Retrieve conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $columns
     * @param  array $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByPaginate(array $conditions, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        $this->retrieveQuery($columns, $options)->where($conditions)->paginate($this->model->getPerPage());
    }

    /**
     * Retrieve all optional conditioning data from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveByOptional(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->where($conditions)->orWhere($orConditions)->get();
    }

    /**
     * Retrieve optional conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByOptionalPaginate(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->where($conditions)->orWhere($orConditions)->paginate($this->model->getPerPage());
    }

    /**
     * Retrieve all data with their defined relation from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveWithRelations(array $relations, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->with($relations)->get();
    }

    /**
     * Retrieve data with their defined relation paginated from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveWithRelationsPaginate(array $relations, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->with($relations)->paginate($this->model->getPerPage());
    }

    /**
     * Retrieve all joined data ASC from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoined(array $joins, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->get();
    }

    /**
     * Retrieve joined data paginated from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedPaginate(array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->paginate($this->model->getPerPage());
    }

    /**
     * Retrieve all conditioning joined data from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoinedBy(array $conditions, array $joins, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->where($conditions)->get();
    }

    /**
     * Retrieve all conditioning joined data from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedByPaginate(array $conditions, array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->where($conditions)->paginate($this->model->getPerPage());
    }

    /**
     * Find the given id.
     * @param int $id
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find(int $id, array $columns = ['*'], bool $trashed = false): Model
    {
        $query = $this->select($columns);
        if (method_exists($this->model, 'withTrashed')) {
            $query->withTrashed($trashed);
        }

        return $query->find($id);
    }

    /**
     * Find the given id or fail.
     * @param int $id
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     *
     */
    public function findOrFail(int $id, array $columns = ['*'], bool $trashed = false): Model
    {
        $query = $this->select($columns);
        if (method_exists($this->model, 'withTrashed')) {
            $query->withTrashed($trashed);
        }

        return $query->findOrFail($id);
    }

    /**
     * Create new record.
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    /**
     * Update the given record id.
     * @param int   $id
     * @param array $data
     * @return int
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(int $id, array $data): int
    {
        $model = $this->findOrFail($id);

        return $model->update($data);
    }

    /**
     * Update the given records ids.
     * @param array $ids
     * @param array $data
     * @return int
     */
    public function updateMany(array $ids, array $data): int
    {
        return $this->model->whereIn($this->model->getKeyName(), $ids)->update($data);
    }

    /**
     * Update data by conditions.
     * @param array $conditions
     * @param array $data
     * @return int
     */
    public function updateBy(array $conditions, array $data): int
    {
        return $this->model->where($conditions)->update($data);
    }

    /**
     * Destroy the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function destroy(int $id):? bool
    {
        $model = $this->findOrFail($id);

        return $model->delete();
    }

    /**
     * Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function destroyMany(array $ids): bool
    {
        return $this->builder()->whereIn($this->model->getKeyName(), $ids)->delete();
    }

    /**
     * Restore the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function restore(int $id):? bool
    {
        $model = $this->findOrFail($id, [$this->model->getKeyName()], true);

        return $model->restore();
    }

    /**
     * Restore the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function restoreMany(array $ids): bool
    {
        return $this->builder()->whereIn($this->model->getKeyName(), $ids)->restore();
    }

    /**
     * Force Destroy the given record id.
     * @param int $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function forceDestroy(int $id):? bool
    {
        $model = $this->findOrFail($id, [$this->model->getKeyName()], true);

        return $model->forceDelete();
    }

    /**
     * Force Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function forceDestroyMany(array $ids): bool
    {
        return $this->builder()->whereIn($this->model->getKeyName(), $ids)->forceDelete();
    }

    /**
     * Checker method to search for the given method on model before throws an exception.
     * @param  string $method
     * @param  mixed  $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (method_exists($this->model, $method)) {
            return $this->model->{$method}(...$arguments);
        }

        throw new \Exception(sprintf("Can't find method (%s) on %s or its model ", $method, static::class));
    }

    /**
     * Try to get the given dependency
     * @param  string $dependency
     * @return mixed
     */
    public function __get($dependency)
    {
        if (!method_exists($this, $dependency)) {
            throw new \Exception(sprintf(
                "Call undefined (%s) property. [Tip] try to use setModel() method in your constructor.",
                $dependency
            ));
        }

        return $this->{$dependency}();
    }
}
