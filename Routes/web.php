<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Afaqy\Core\Traits\FileProcessing;
use Intervention\Image\Facades\Image;
use League\Flysystem\FileNotFoundException;

Route::get('image/{dir}/{image}', function ($dir, $image) {
    $imageFile = FileProcessing::generateImageURL($dir . '/' . $image);
    if (!$imageFile) {
        throw new FileNotFoundException();
    }

    try {
        return Image::make(Image::make($imageFile['file']))->response();
    } catch (Exception $exception) {
        return response()->download($imageFile['file']);
    }
})->name('image.show');

Route::get('file/{dir}/{image}/{name?}', function ($dir, $file, $name = '') {
    try {
        return response()->download(storage_path('media/'.$dir.'/'.$file), $name);
    } catch (Exception $exception) {
        throw new FileNotFoundException();
    }
})->name('file.show');
