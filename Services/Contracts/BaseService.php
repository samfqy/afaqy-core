<?php

namespace Afaqy\Core\Services\Contracts;

use Afaqy\Core\Caches\Contracts\BaseCache;
use Afaqy\Core\Repositories\Contracts\BaseRepository;

interface BaseService
{
    /**
     * Set cache.
     * @param \Afaqy\Core\Caches\Contracts\BaseCache $cache
     */
    public function setCache(BaseCache $cache): void;

    /**
     * Get the cache object.
     * @return \Afaqy\Core\Caches\Contracts\BaseCache
     */
    public function cache(): BaseCache;
}
