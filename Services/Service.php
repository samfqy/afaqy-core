<?php

namespace Afaqy\Core\Services;

use Afaqy\Core\Caches\Contracts\BaseCache;
use Afaqy\Core\Services\Contracts\BaseService;
use Afaqy\Core\Repositories\Contracts\BaseRepository;

abstract class Service implements BaseService
{
    /**
     * Repository class.
     * @var \Afaqy\Core\Repositories\Contracts\BaseRepository
     */
    private $repository;

    /**
     * Cache class.
     * @var \Afaqy\Core\Caches\Contracts\BaseCache
     */
    private $cache;

    /**
     * Set repository.
     * @param \Afaqy\Core\Repositories\Contracts\BaseRepository $repository
     */
    public function setRepo(BaseRepository $repository): void
    {
        $this->repository = $repository;
    }

    /**
     * Get the repository object.
     * @return \Afaqy\Core\Repositories\Contracts\BaseRepository
     */
    public function repo(): BaseRepository
    {
        return $this->repository;
    }

    /**
     * Set cache.
     * @param \Afaqy\Core\Caches\Contracts\BaseCache $cache
     */
    public function setCache(BaseCache $cache): void
    {
        $this->cache = $cache;
    }

    /**
     * Get the cache object.
     * @return \Afaqy\Core\Caches\Contracts\BaseCache
     */
    public function cache(): BaseCache
    {
        return $this->cache;
    }

    /**
     * Try to find the called method in repository layer
     * @param  string $method
     * @param  array $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (method_exists($this->repo(), $method)) {
            return $this->repo()->{$method}(...$arguments);
        }

        throw new \Exception(sprintf("Can't find method (%s) on %s class.", $method, static::class));
    }

    /**
     * Try to get the given dependency
     * @param  string $dependency
     * @return mixed
     */
    public function __get($dependency)
    {
        if (!method_exists($this, $dependency)) {
            throw new \Exception(sprintf(
                "Call undefined (%s) property. [Tip] try to use setRepo() or setCache() methods in your constructor.",
                $dependency
            ));
        }

        return $this->{$dependency}();
    }
}
