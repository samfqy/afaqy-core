<?php

namespace Afaqy\Core\Tests;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Animal extends Eloquent
{
    protected $table = 'animals';

    protected $guarded = [];

    protected $hidden = ['updated_at', 'deleted_at'];

    public function translations()
    {
        return $this->hasMany(AnimalTranslation::class, 'animal_id');
    }

    public function family()
    {
        return $this->belongsTo(Family::class, 'family_id');
    }
}
