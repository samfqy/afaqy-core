<?php

namespace Afaqy\Core\Tests;

use Illuminate\Database\Eloquent\Model as Eloquent;

class AnimalJoin extends Eloquent
{
    protected $table = 'animals';

    protected $guarded = [];

    public function scopeTranslations($query)
    {
        return $query->join('animal_translations', 'animals.id', '=', 'animal_translations.animal_id');
    }

    public function scopeFamily($query)
    {
        return $query->join('families', 'animals.family_id', '=', 'families.id');
    }
}
