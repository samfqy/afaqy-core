<?php

namespace Afaqy\Core\Tests;

use Illuminate\Database\Eloquent\Model as Eloquent;

class AnimalTranslation extends Eloquent
{
    protected $table = 'animal_translations';

    protected $guarded = [];

    public $timestamps = false;

    public function animal()
    {
        return $this->belongsTo(Animal::class, 'animal_id');
    }
}
