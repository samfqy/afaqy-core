<?php

namespace Afaqy\Core\Tests;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Family extends Eloquent
{
    protected $table = 'families';

    protected $guarded = [];

    public $timestamps = false;

    public function animal()
    {
        return $this->hasMany(Animal::class, 'family_id');
    }
}
