<?php

namespace Afaqy\Core\Tests\Unit\BaseRepository;

use Tests\TestCase;
use Afaqy\Core\Tests\Animal;
use Afaqy\Core\Repositories\Repository;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BaseRepositoryDeleteTest extends TestCase
{
    protected $concrete;

    protected function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class(new Animal()) extends Repository {
        };

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('family');
            $table->timestamps();
            $table->softDeletes();
        });
        // it just an example, if you ask why name or family not in translation table : )
        $this->schema()->create('animal_translations', function ($table) {
            $table->increments('id');
            $table->unsignedBigInteger('animal_id');
            $table->string('details');
            $table->enum('language', ['en', 'ar']);
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
        $this->schema()->drop('animal_translations');
    }

    public function testDestroyMethodDeleteTheGivenIDData()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Cat', 'family' => 'Felidae'],
            ['id' => 3, 'name' => 'Dog', 'family' => 'Canis'],
        ]);

        $this->concrete->destroy(1);

        $animal_data = Animal::all();

        $this->assertCount(2, $animal_data);
    }

    public function testDestroyMethodThrowsNotFoundExceptionIfGivenIDNotFound()
    {
        $this->expectException(\Illuminate\Database\Eloquent\ModelNotFoundException::class);

        $this->concrete->destroy(15);
    }

    public function testDestroyManyMethodDeleteTheGivenIdsData()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Cat', 'family' => 'Felidae'],
            ['id' => 3, 'name' => 'Dog', 'family' => 'Canis'],
        ]);

        $this->concrete->destroyMany([1, 2]);

        $animal_data = Animal::all();

        $this->assertCount(1, $animal_data);
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
