<?php

namespace Afaqy\Core\Tests\Unit\BaseRepository;

use Tests\TestCase;
use Illuminate\Support\Arr;
use Afaqy\Core\Tests\Animal;
use Afaqy\Core\Repositories\Repository;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BaseRepositoryInsertionTest extends TestCase
{
    protected $concrete;

    protected function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class(new Animal()) extends Repository {
        };

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('family');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
    }

    public function testCreateMethodSaveTheGivenData()
    {
        $this->concrete->create([
            'name'     => 'Lion',
            'family'   => 'Felidae',
        ]);

        $this->assertDatabaseHas('animals', [
            'name'     => 'Lion',
            'family'   => 'Felidae',
        ]);
    }

    public function testCreateMethodReturnsTheSavedData()
    {
        $hotel = $this->concrete->create([
            'name'     => 'Lion',
            'family'   => 'Felidae',
        ])->toArray();

        $this->assertSame([
            'name'     => 'Lion',
            'family'   => 'Felidae',
        ], Arr::only($hotel, ['name', 'family']));
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
