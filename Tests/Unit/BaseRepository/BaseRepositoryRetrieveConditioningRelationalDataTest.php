<?php

namespace Afaqy\Core\Tests\Unit\BaseRepository;

use Carbon\Carbon;
use Tests\TestCase;
use Carbon\CarbonImmutable;
use Afaqy\Core\Tests\Family;
use Afaqy\Core\Tests\AnimalJoin;
use Afaqy\Core\Repositories\Repository;
use Afaqy\Core\Tests\AnimalTranslation;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BaseRepositoryRetrieveConditioningRelationalDataTest extends TestCase
{
    protected $concrete;

    protected function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedBigInteger('family_id');
            $table->string('home');
            $table->timestamps();
        });

        $this->schema()->create('animal_translations', function ($table) {
            $table->increments('id');
            $table->unsignedBigInteger('animal_id');
            $table->string('details');
            $table->enum('language', ['en', 'ar']);
        });

        $this->schema()->create('families', function ($table) {
            $table->increments('id');
            $table->string('name');
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
        $this->schema()->drop('animal_translations');
        $this->schema()->drop('families');
    }

    public function testRetrieveJoinedByMethodCanReturnSpecificColumns()
    {
        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotel_data = $this->concrete->retrieveJoinedBy(
            [
                ['animals.name', 'like', '%Dog%'],
            ],
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language']
        )->first()->toArray();

        $this->assertArrayNotHasKey('home', $hotel_data);
    }

    public function testRetrieveJoinedByMethodReturnsConditioningDataOrderedAscendingByCreateTime()
    {
        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(2, 'day')->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a five strong animal.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoinedBy(
            [
                ['animals.home', 'like', '%Anywhere%'],
            ],
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language'],
            ['sort' => 'id', 'direction' => 'asc']
        )->toArray();

        $this->assertEquals(
            [2, 3],
            [
                $hotels_data[0]['id'],
                $hotels_data[1]['id'],
            ]
        );
    }

    public function testRetrieveJoinedByMethodReturnsDataForLoadedRelationshipMethodsNames()
    {
        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(2, 'day')->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a five strong animal.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoinedBy(
            [
                ['animals.home', 'like', '%Anywhere%'],
            ],
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language']
        )->toArray();

        $this->assertArrayHasKey('name', $hotels_data[0]);
        $this->assertArrayHasKey('details', $hotels_data[0]); // it load translation relationship automatically behind the scene
        $this->assertArrayHasKey('family_name', $hotels_data[0]);
    }

    public function testRetrieveLatestJoinedByMethodCanReturnSpecificColumns()
    {
        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotel_data = $this->concrete->retrieveJoinedBy(
            [
                ['animals.name', 'like', '%Dog%'],
            ],
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language'],
            ['sort' => 'id', 'direction' => 'desc']
        )->first()->toArray();

        $this->assertArrayNotHasKey('home', $hotel_data);
    }

    public function testRetrieveLatestJoinedByMethodReturnsConditioningDataOrderedAscendingByCreateTime()
    {
        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(2, 'day')->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a five strong animal.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoinedBy(
            [
                ['animals.home', 'like', '%Anywhere%'],
            ],
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language'],
            ['sort' => 'id', 'direction' => 'desc']
        )->toArray();

        $this->assertEquals(
            [3, 2],
            [
                $hotels_data[0]['id'],
                $hotels_data[1]['id'],
            ]
        );
    }

    public function testRetrieveLatestJoinedByMethodReturnsDataForLoadedRelationshipMethodsNames()
    {
        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(2, 'day')->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a five strong animal.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoinedBy(
            [
                ['animals.home', 'like', '%Anywhere%'],
            ],
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language'],
            ['sort' => 'id', 'direction' => 'desc']
        )->toArray();

        $this->assertArrayHasKey('name', $hotels_data[0]);
        $this->assertArrayHasKey('details', $hotels_data[0]); // it load translation relationship automatically behind the scene
        $this->assertArrayHasKey('family_name', $hotels_data[0]);
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
