<?php

namespace Afaqy\Core\Tests\Unit\BaseRepository;

use Carbon\Carbon;
use Tests\TestCase;
use Carbon\CarbonImmutable;
use Afaqy\Core\Tests\Animal;
use Afaqy\Core\Tests\AnimalJoin;
use Afaqy\Core\Repositories\Repository;
use Afaqy\Core\Tests\AnimalTranslation;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BaseRepositoryRetrieveDataTest extends TestCase
{
    protected $concrete;

    protected function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class(new Animal()) extends Repository {
        };

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('family');
            $table->timestamps();
            $table->softDeletes();
        });
        // it just an example, if you ask why name or family not in translation table : )
        $this->schema()->create('animal_translations', function ($table) {
            $table->increments('id');
            $table->unsignedBigInteger('animal_id');
            $table->string('details');
            $table->enum('language', ['en', 'ar']);
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
        $this->schema()->drop('animal_translations');
    }

    public function testRetrieveMethodCanReturnSpecificColumns()
    {
        Animal::create(['id' => 1, 'name' => 'Lion', 'family' => 'Felidae']);

        $animal_data = $this->concrete->retrieve(['name', 'family'])->first()->toArray();

        $this->assertArrayNotHasKey('id', $animal_data);
    }

    public function testRetrieveMethodByDefaultReturnsDataOrderedAscendingByCreateTime()
    {
        $data = [
            ['id' => 1, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => Carbon::now()->toDateTimeString()],
        ];
        Animal::insert($data);

        $animal_data = $this->concrete->retrieve()->makeHidden(['translations'])->toArray();

        $data = collect($data)->sortBy('created_at')->values()->all();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveMethodCanReturnDataOrderedAscendingByAnyColumn()
    {
        $data = [
            ['id' => 1, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => Carbon::now()->toDateTimeString()],
        ];
        Animal::insert($data);

        $animal_data = $this->concrete->retrieve(['*'], ['sort' => 'id', 'direction' => 'asc'])->makeHidden(['translations'])->toArray();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveMethodReturnsTranslatedDataAutomaticallyByEagerLoadingIfExist()
    {
        $animal      = Animal::create(['id' => 1, 'name' => 'Lion', 'family' => 'Felidae']);
        $translation = new AnimalTranslation(['details' => 'The lion is a species in the family Felidae', 'language' => 'en']);
        $animal->translations()->save($translation);

        $animal_data = $this->concrete->retrieve()->toArray();

        $this->assertArrayHasKey('translations', $animal_data[0]);
    }

    public function testRetrieveMethodReturnsTranslatedDataAutomaticallyThroughTranslationsScopeMethodIfNoEagerLoadingFound()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        $animal      = AnimalJoin::create(['id' => 1, 'name' => 'Lion', 'family' => 'Felidae']);
        $translation = AnimalTranslation::create(['animal_id' => $animal->id, 'details' => 'The lion is a species in the family Felidae', 'language' => 'en']);

        $animal_data = $this->concrete->retrieve()->toArray();

        $this->assertArrayHasKey('details', $animal_data[0]);
    }

    public function testRetrieveLatestMethodCanReturnSpecificColumns()
    {
        Animal::create(['id' => 1, 'name' => 'Lion', 'family' => 'Felidae']);

        $animal_data = $this->concrete->retrieve(['name', 'family'], ['direction' => 'desc'])->first()->toArray();

        $this->assertArrayNotHasKey('id', $animal_data);
    }

    public function testRetrieveLatestMethodByDefaultReturnsDataOrderedDescendingByCreateTime()
    {
        $data = [
            ['id' => 1, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => Carbon::now()->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieve(['*'], ['sort' => 'created_at', 'direction' => 'desc'])->makeHidden(['translations'])->toArray();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveLatestMethodCanReturnDataOrderedAscendingByAnyColumn()
    {
        $data = [
            ['id' => 1, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => Carbon::now()->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieve(['*'], ['sort' => 'id', 'direction' => 'asc'])->makeHidden(['translations'])->toArray();

        $data = collect($data)->sortBy('id')->values()->all();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveLatestMethodReturnsTranslatedDataAutomaticallyByEagerLoadingIfExist()
    {
        $animal      = Animal::create(['id' => 1, 'name' => 'Lion', 'family' => 'Felidae']);
        $translation = new AnimalTranslation(['details' => 'The lion is a species in the family Felidae', 'language' => 'en']);
        $animal->translations()->save($translation);

        $animal_data = $this->concrete->retrieve(['*'], ['direction' => 'desc'])->toArray();

        $this->assertArrayHasKey('translations', $animal_data[0]);
    }

    public function testRetrieveLatestMethodReturnsTranslatedDataAutomaticallyThroughTranslationsScopeMethodIfNoEagerLoadingFound()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        $animal      = AnimalJoin::create(['id' => 1, 'name' => 'Lion', 'family' => 'Felidae']);
        $translation = AnimalTranslation::create(['animal_id' => $animal->id, 'details' => 'The lion is a species in the family Felidae', 'language' => 'en']);

        $animal_data = $this->concrete->retrieve(['*'], ['direction' => 'desc'])->toArray();

        $this->assertArrayHasKey('details', $animal_data[0]);
    }

    public function testFindMethodReturnsDataForTheGivenID()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        $animal_data = $this->concrete->find(2)->toArray();

        $this->assertEquals(2, $animal_data['id']);
    }

    public function testFindOrFailMethodReturnsDataForTheGivenID()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        $animal_data = $this->concrete->findOrFail(2)->toArray();

        $this->assertEquals(2, $animal_data['id']);
    }

    public function testFindOrFailMethodThrowsNotFoundExceptionIfGivenIDNotFound()
    {
        $this->expectException(\Illuminate\Database\Eloquent\ModelNotFoundException::class);

        Animal::insert([
            ['id' => 1, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
        ]);

        $this->concrete->findOrFail(2)->toArray();
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
