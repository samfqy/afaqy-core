<?php

namespace Afaqy\Core\Tests\Unit\BaseRepository;

use Carbon\Carbon;
use Tests\TestCase;
use Carbon\CarbonImmutable;
use Afaqy\Core\Tests\Animal;
use Afaqy\Core\Tests\Family;
use Afaqy\Core\Tests\AnimalJoin;
use Afaqy\Core\Repositories\Repository;
use Afaqy\Core\Tests\AnimalTranslation;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BaseRepositoryRetrieveRelationalDataTest extends TestCase
{
    protected $concrete;

    protected function setUp(): void
    {
        parent::setUp();

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedBigInteger('family_id');
            $table->string('home');
            $table->timestamps();
        });

        $this->schema()->create('animal_translations', function ($table) {
            $table->increments('id');
            $table->unsignedBigInteger('animal_id');
            $table->string('details');
            $table->enum('language', ['en', 'ar']);
        });

        $this->schema()->create('families', function ($table) {
            $table->increments('id');
            $table->string('name');
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
        $this->schema()->drop('animal_translations');
        $this->schema()->drop('families');
    }

    public function testRetrieveWithRelationsMethodCanReturnSpecificColumns()
    {
        $this->concrete = new class(new Animal()) extends Repository {
        };

        Animal::create(
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa']
        );

        AnimalTranslation::create(
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en']
        );

        Family::create(
            ['id' => 1, 'name' => 'Felidae']
        );

        $animal_data = $this->concrete->retrieveWithRelations(
            ['family'],
            ['id', 'name', 'family_id']
        )->first()->makeHidden(['translations'])->toArray();

        $this->assertArrayNotHasKey('home', $animal_data);
    }

    public function testRetrieveWithRelationsMethodReturnsDataOrderedAscendingByCreateTime()
    {
        $this->concrete = new class(new Animal()) extends Repository {
        };

        Animal::insert([
            ['id' => 1, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveWithRelations(['family'])->toArray();

        $this->assertEquals(
            [2, 1],
            [
                $hotels_data[0]['id'],
                $hotels_data[1]['id'],
            ]
        );
    }

    public function testRetrieveWithRelationsMethodReturnsDataForLoadedRelationshipMethodsNames()
    {
        $this->concrete = new class(new Animal()) extends Repository {
        };

        Animal::insert([
            ['id' => 1, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveWithRelations(['family'])->toArray();

        $this->assertArrayHasKey('name', $hotels_data[0]);
        $this->assertArrayHasKey('family', $hotels_data[0]);
        $this->assertArrayHasKey('translations', $hotels_data[0]); // it load translation relationship automatically behind the scene
    }

    public function testRetrieveLatestWithRelationsMethodCanReturnSpecificColumns()
    {
        $this->concrete = new class(new Animal()) extends Repository {
        };

        Animal::create(
            ['id' => 2, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa']
        );

        AnimalTranslation::create(
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en']
        );

        Family::create(
            ['id' => 1, 'name' => 'Felidae']
        );

        $animal_data = $this->concrete->retrieveWithRelations(
            ['family'],
            ['id', 'name', 'family_id'],
            ['direction' => 'desc']
        )->first()->toArray();

        $this->assertArrayNotHasKey('home', $animal_data);
    }

    public function testRetrieveLatestWithRelationsMethodReturnsDataOrderedDescendingByCreateTime()
    {
        $this->concrete = new class(new Animal()) extends Repository {
        };

        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveWithRelations(['family'], ['*'], ['direction' => 'desc'])->toArray();

        $this->assertEquals(
            [2, 1],
            [
                $hotels_data[0]['id'],
                $hotels_data[1]['id'],
            ]
        );
    }

    public function testRetrieveLatestWithRelationsMethodReturnsDataForLoadedRelationshipMethodsNames()
    {
        $this->concrete = new class(new Animal()) extends Repository {
        };

        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveWithRelations(['family'], ['*'], ['direction' => 'desc'])->toArray();

        $this->assertArrayHasKey('name', $hotels_data[0]);
        $this->assertArrayHasKey('family', $hotels_data[0]);
        $this->assertArrayHasKey('translations', $hotels_data[0]); // it load translation relationship automatically behind the scene
    }

    public function testRetrieveJoinedMethodCanReturnSpecificColumns()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::create(
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa']
        );

        AnimalTranslation::create(
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en']
        );

        Family::create(
            ['id' => 1, 'name' => 'Felidae']
        );

        $animal_data = $this->concrete->retrieveJoined(
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language']
        )->first()->toArray();

        $this->assertArrayNotHasKey('home', $animal_data);
    }

    public function testRetrieveJoinedMethodReturnsDataOrderedAscendingByCreateTime()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoined(
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language']
        )->toArray();

        $this->assertEquals(
            [2, 1],
            [
                $hotels_data[0]['id'],
                $hotels_data[1]['id'],
            ]
        );
    }

    public function testRetrieveJoinedMethodReturnsDataForLoadedJoinMethodsNames()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoined(
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language']
        )->toArray();

        $this->assertArrayHasKey('name', $hotels_data[0]);
        $this->assertArrayHasKey('details', $hotels_data[0]); // it load translation relationship automatically behind the scene
        $this->assertArrayHasKey('family_name', $hotels_data[0]);
    }

    public function testRetrieveLatestJoinedMethodCanReturnSpecificColumns()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::create(
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa']
        );

        AnimalTranslation::create(
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en']
        );

        Family::create(
            ['id' => 1, 'name' => 'Felidae']
        );

        $animal_data = $this->concrete->retrieveJoined(
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language'],
            ['direction' => 'desc']
        )->first()->toArray();

        $this->assertArrayNotHasKey('home', $animal_data);
    }

    public function testRetrieveLatestJoinedMethodReturnsDataOrderedDescendingByCreateTime()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 2, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 1, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoined(
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language'],
            ['direction' => 'desc']
        )->toArray();

        $this->assertEquals(
            [2, 1],
            [
                $hotels_data[0]['id'],
                $hotels_data[1]['id'],
            ]
        );
    }

    public function testRetrieveLatestJoinedMethodReturnsDataForLoadedJoinMethodsNames()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Dog', 'family_id' => 2, 'home' => 'Anywhere', 'created_at' => CarbonImmutable::now()->add(1, 'day')->toDateTimeString()],
            ['id' => 2, 'name' => 'Lion', 'family_id' => 1, 'home' => 'Africa', 'created_at' => Carbon::now()->toDateTimeString()],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five stars animals.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three stars animals.', 'language' => 'en'],
        ]);

        Family::insert([
            ['id' => 1, 'name' => 'Felidae'],
            ['id' => 2, 'name' => 'Canis'],
        ]);

        $hotels_data = $this->concrete->retrieveJoined(
            ['family'],
            ['animals.id', 'animals.name', 'animal_translations.details', 'families.name as family_name', 'animal_translations.language'],
            ['direction' => 'desc']
        )->toArray();

        $this->assertArrayHasKey('name', $hotels_data[0]);
        $this->assertArrayHasKey('details', $hotels_data[0]); // it load translation relationship automatically behind the scene
        $this->assertArrayHasKey('family_name', $hotels_data[0]);
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
