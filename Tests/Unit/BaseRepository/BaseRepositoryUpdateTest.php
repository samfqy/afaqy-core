<?php

namespace Afaqy\Core\Tests\Unit\BaseRepository;

use Tests\TestCase;
use Afaqy\Core\Tests\Animal;
use Afaqy\Core\Repositories\Repository;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BaseRepositoryUpdateTest extends TestCase
{
    protected $concrete;

    protected function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class(new Animal()) extends Repository {
        };

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('family');
            $table->timestamps();
            $table->softDeletes();
        });
        // it just an example, if you ask why name or family not in translation table : )
        $this->schema()->create('animal_translations', function ($table) {
            $table->increments('id');
            $table->unsignedBigInteger('animal_id');
            $table->string('details');
            $table->enum('language', ['en', 'ar']);
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
        $this->schema()->drop('animal_translations');
    }

    public function testUpdateMethodUpdateTheGivenIDData()
    {
        Animal::create(['id' => 1, 'name' => 'Lionss', 'family' => 'Felidae']);
        $this->concrete->update(1, ['name' => 'Lion']);
        $this->assertDatabaseHas('animals', ['name' => 'Lion', 'family' => 'Felidae']);
    }

    public function testUpdateMethodThrowsNotFoundExceptionIfGivenIDNotFound()
    {
        $this->expectException(\Illuminate\Database\Eloquent\ModelNotFoundException::class);
        $this->concrete->update(15, ['name' => 'Lion']);
    }

    public function testUpdateManyMethodUpdateTheGivenIdsData()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'wolf', 'family' => 'Canis lupus'],
        ]);

        $this->concrete->updateMany([1, 2], ['family' => 'Felidae']);

        $this->assertDatabaseHas('animals', [
            'name'     => 'Lion',
            'family'   => 'Felidae',
        ])->assertDatabaseHas('animals', [
            'name'     => 'Dog',
            'family'   => 'Felidae',
        ]);
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
