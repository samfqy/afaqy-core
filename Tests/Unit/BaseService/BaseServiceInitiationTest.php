<?php

namespace Afaqy\Core\Tests\Unit\BaseService;

use Mockery;
use Tests\TestCase;
use Afaqy\Core\Services\Service;
use Afaqy\Core\Caches\Contracts\BaseCache;
use Afaqy\Core\Services\Contracts\BaseService;
use Afaqy\Core\Repositories\Contracts\BaseRepository;

class BaseServiceInitiationTest extends TestCase
{
    protected $concrete;

    protected function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class extends Service {
        };
    }

    public function testExtendedConcreteClassIsAnInstanceOfBaseServiceInterface()
    {
        $this->assertInstanceOf(
            BaseService::class,
            $this->concrete
        );
    }

    public function testItCanSetARepository()
    {
        $TestObjectRepository = Mockery::mock(BaseRepository::class);

        $this->concrete->setRepo($TestObjectRepository);

        $this->assertInstanceOf(
            BaseRepository::class,
            $this->concrete->repo()
        );
    }

    public function testItCanCallInjectedRepositoryAsAMethod()
    {
        $TestObjectRepository = Mockery::mock(BaseRepository::class);

        $this->concrete->setRepo($TestObjectRepository);

        $this->assertInstanceOf(
            BaseRepository::class,
            $this->concrete->repo()
        );
    }

    public function testItCanCallInjectedRepositoryAsAProperty()
    {
        $TestObjectRepository = Mockery::mock(BaseRepository::class);

        $this->concrete->setRepo($TestObjectRepository);

        $this->assertInstanceOf(
            BaseRepository::class,
            $this->concrete->repo
        );
    }

    public function testItCanSetACacheRepository()
    {
        $TestObjectRepository = Mockery::mock(BaseCache::class);

        $this->concrete->setCache($TestObjectRepository);

        $this->assertInstanceOf(
            BaseCache::class,
            $this->concrete->cache()
        );
    }

    public function testItCanCallInjectedCacheRepositoryAsAMethod()
    {
        $TestObjectRepository = Mockery::mock(BaseCache::class);

        $this->concrete->setCache($TestObjectRepository);

        $this->assertInstanceOf(
            BaseCache::class,
            $this->concrete->cache()
        );
    }

    public function testItCanCallInjectedCacheRepositoryAsAProperty()
    {
        $TestObjectRepository = Mockery::mock(BaseCache::class);

        $this->concrete->setCache($TestObjectRepository);

        $this->assertInstanceOf(
            BaseCache::class,
            $this->concrete->cache
        );
    }

    public function testGetMagicMethodThrowsAnExceptionWhenCallPropertyNotCorrespondedToAnyMethodNameInService()
    {
        $this->expectException(\Exception::class);

        $this->concrete->anyRandomMethodName;
    }
}
