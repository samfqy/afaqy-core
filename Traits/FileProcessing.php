<?php

namespace Afaqy\Core\Traits;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;

trait FileProcessing
{
    public static function uploadFile($request, $dir, $input = 'image')
    {
        return str_replace($dir.'/', '', $request->file($input)->store($dir));
    }

    public static function generateImageURL($path)
    {
        $image    = [];
        $fullPath = storage_path('media/'.$path);
        if (\File::exists($fullPath)) {
            $image['file'] = \File::get($fullPath);
            $image['type'] = \File::mimeType($fullPath);

            return $image;
        } else {
            throw new FileNotFoundException();
        }
    }
}
