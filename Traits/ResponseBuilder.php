<?php

namespace Afaqy\Core\Traits;

use Illuminate\Http\JsonResponse;
use ArrayKeysCaseTransform\ArrayKeys;

trait ResponseBuilder
{
    /**
     * Returned data or errors.
     * @var array
     */
    private $responseData = [];

    /**
     * Response message.
     * @var string|null
     */
    private $responseMessage = '';

    /**
     * Header codes.
     * @var integer
     */
    private $header_code;

    /**
     * Internal error codes.
     * @var integer|null
     */
    private $internal_code;

    /**
     * Error exception.
     * @var \Exception
     */
    private $responseException;

    /**
     * Return success response.
     * @param  string $message
     * @param  array  $data
     * @param  integer $headerCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnSuccess(string $message = 'ok', array $data = [], int $headerCode = 200): JsonResponse
    {
        return $this->setResponseMessage($message)
            ->setResponseData($data)
            ->success($headerCode);
    }

    /**
     * Return error response.
     * @param  integer $headerCode
     * @param  null|string $message
     * @param  array  $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnError(int $headerCode, ?string $message = '', array $data = []): JsonResponse
    {
        return $this->setResponseMessage($message)
            ->setResponseData($data)
            ->error($headerCode);
    }

    /**
     * Return bad request response.
     * @param  null|integer $internalCode
     * @param  null|string $message
     * @param  array  $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnBadRequest(?int $internalCode = null, ?string $message = '', array $data = []): JsonResponse
    {
        return $this->setResponseCode($internalCode)
            ->setResponseMessage($message)
            ->setResponseData($data)
            ->error(400);
    }

    /**
     * Set the given exception.
     * @param  \Exception $exception
     * @return $this
     */
    public function withResponseException(\Exception $exception): self
    {
        $this->responseException = $exception;

        return $this;
    }

    /**
     * Set internal error codes.
     * @param integer|null $code
     * @return $this
     */
    protected function setResponseCode(?int $code): self
    {
        $this->internal_code = $code;

        return $this;
    }

    /**
     * Set response message.
     * @param string|null $message
     * @return $this
     */
    protected function setResponseMessage(?string $message): self
    {
        $this->responseMessage = $message;

        return $this;
    }

    /**
     * Set response data.
     * @param array $data
     * @return $this
     */
    protected function setResponseData(array $data): self
    {
        $this->responseData = $data;

        return $this;
    }

    /**
     * Get response exception.
     *
     * @SuppressWarnings("StaticAccess")
     *
     * @return \Exception|null
     */
    public function responseException():? \Exception
    {
        return $this->responseException;
    }

    /**
     * Get formated exception data.
     * @return array
     */
    public function responseDebug(): array
    {
        return [
            'exceptionMessage' => $this->responseException->getMessage(),
            'exceptionFile'    => $this->responseException->getFile(),
            'exceptionLine'    => $this->responseException->getLine(),
        ];
    }

    /**
     * Get internal error codes.
     * @return integer|null
     */
    protected function responseCode():? int
    {
        return $this->internal_code;
    }

    /**
     * Get response message.
     * @return string|null
     */
    protected function responseMessage():? string
    {
        return $this->responseMessage;
    }

    /**
     * Get response data.
     * @return array
     */
    protected function responseData(): array
    {
        return $this->formatFloatNumbers($this->responseData);
    }

    /**
     * Format any float number inside the array.
     * @return array
     */
    private function formatFloatNumbers($data)
    {
        return collect($data)->map(function ($item) {
            if (is_float($item)) {
                return preg_replace('/\.00/', '', number_format((float) $item, 2, '.', ''));
            } elseif (is_array($item)) {
                return $this->formatFloatNumbers($item);
            }

            return $item;
        })->toArray();
    }

    /**
     * Build error response.
     * @param  integer  $headerCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function error(int $headerCode): JsonResponse
    {
        $response = [];

        if (!empty($this->responseCode())) {
            $response['code'] = $this->responseCode();
        }

        $response['message'] = $this->responseMessage();

        $response['errors'] = $this->responseData();

        if (config('app.debug') == true && !empty($this->responseException())) {
            $response['debug'] = $this->responseDebug();
        }

        return $this->response($response, $headerCode);
    }

    /**
     * Build success response.
     * @param  integer  $headerCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function success(int $headerCode): JsonResponse
    {
        $response = [
            'message'=> $this->responseMessage(),
            'data'   => $this->responseData(),
        ];

        if (isset($this->responseData()['data'])) {
            $response['data'] = $this->responseData()['data'];
        }

        if (isset($this->responseData()['additionalData'])) {
            $response['additionalData'] = $this->responseData()['additionalData'];
        }

        if (isset($this->responseData()['meta'])) {
            $response['meta'] = $this->responseData()['meta'];
        }

        return $this->response($response, $headerCode);
    }

    /**
     * Prepare returned response.
     * @param  array $response    [description]
     * @param  integer $headerCode [description]
     *
     * @SuppressWarnings("StaticAccess")
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function response(array $response, int $headerCode): JsonResponse
    {
        return response()->json(ArrayKeys::toCamelCase($response), $headerCode);
    }
}
